package com.freecharge.spring.mybatis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.freecharge.spring.mybatis.entity.Employee;
import com.freecharge.spring.mybatis.repository.EmployeeRepository;


@Controller
public class EmployeeController {
	
	@Autowired	
	EmployeeRepository employeeRepository;

	@RequestMapping("/")
	public String getHomePage(ModelMap model){
        model.addAttribute("empList", this.employeeRepository.getEmployeesList());
        return "index";
    }
	
	
	 @PostMapping("/addemployee")
	    public ModelAndView addEmployee(@ModelAttribute("employee") Employee employee) {
	        this.employeeRepository.saveEmployee(employee);
	        ModelAndView modelAndView = new ModelAndView("index");
	        modelAndView.addObject("empList", this.employeeRepository.getEmployeesList());
	        return modelAndView;
	    }
	 
	 @GetMapping("/deleteemployee")
	    public ModelAndView deleteEmployee(@RequestParam("empId") int id) {
	        this.employeeRepository.deleteEmployee(id);
	        ModelAndView modelAndView = new ModelAndView("redirect:/");
	        modelAndView.addObject("empList", this.employeeRepository.getEmployeesList());
	        return modelAndView;
	    }
	 
	 @GetMapping("/getEmpInfo/{id}")
	 public ModelAndView getEmpInfo(@PathVariable("id") int id) {
	        this.employeeRepository.getEmpInfo(id);
	        ModelAndView modelAndView = new ModelAndView("redirect:/");
	        modelAndView.addObject("empList", this.employeeRepository.getEmployeesList());
	        return modelAndView;
	    }
	 
	 
	 @GetMapping("/getEmpInfoBasedOnSalaries/{salary}")
	 public ModelAndView getEmpInfoBasedOnSalaries(@PathVariable("salary") double salary) {
	        this.employeeRepository.getEmpInfoSalaries(salary);
	        ModelAndView modelAndView = new ModelAndView("redirect:/");
	        modelAndView.addObject("empList", this.employeeRepository.getEmployeesList());
	        return modelAndView;
	    }
	 
	 

}
