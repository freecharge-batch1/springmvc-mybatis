package com.freecharge.spring.mybatis.repository;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.freecharge.spring.mybatis.entity.Employee;
import com.freecharge.spring.mybatis.util.MyBatisUtil;

@Repository
public class EmployeeRepository {

	//CRUD Methods
	
	public List<Employee> getEmployeesList(){
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();		
		List<Employee> empList = session.selectList("getAllEmployees");
		session.commit();		
		session.close();
		return empList;
	}

	public int saveEmployee(Employee employee) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();		
		int rows = session.insert("insertEmployee",employee);
		session.commit();		
		session.close();
		return rows;
		
	}
	
	public int deleteEmployee(int id) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();		
		int rows = session.delete("deleteEmployee",id);
		session.commit();		
		session.close();
		return rows;
		
	}

	public void getEmpInfo(int id) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();		
		Employee employee = session.selectOne("getEmpInfo",id);
		System.out.println("empList from stored procedure : " + employee);
		
	}

	public void getEmpInfoSalaries(double salary) {
		SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();		
		List<Employee> employee = session.selectList("getEmpInfoSalaries",salary);
		System.out.println("empList from stored procedure : " + employee);
		
	}
}
