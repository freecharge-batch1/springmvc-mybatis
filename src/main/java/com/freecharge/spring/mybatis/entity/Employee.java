package com.freecharge.spring.mybatis.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Employee {

	private int id;
	private String name;
	private String email;
	private String contactNo;
	private double salary;
	private String department;
	
}
