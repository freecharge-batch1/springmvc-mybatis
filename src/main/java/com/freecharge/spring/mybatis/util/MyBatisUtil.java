package com.freecharge.spring.mybatis.util;

import java.io.Reader;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class MyBatisUtil {
	
	private static SqlSessionFactory sqlSessionFactory;
	
	static {
		
		try (Reader reader = Resources.getResourceAsReader("resources/mybatis-config.xml")){
			sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);
			System.out.println("sql Session Factory : " + sqlSessionFactory);
		} catch(Exception e) {
			e.printStackTrace();
		} 
	}
	
	
	public static SqlSessionFactory getSqlSessionFactory() {
		return sqlSessionFactory;
	}

}
